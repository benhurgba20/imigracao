-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17-Set-2019 às 19:53
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `loja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `moveis`
--

CREATE TABLE IF NOT EXISTS `moveis` (
  `titulo` varchar(500) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `foto1` varchar(500) DEFAULT NULL,
  `foto2` varchar(500) DEFAULT NULL,
  `foto3` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `moveis`
--

INSERT INTO `moveis` (`titulo`, `descricao`, `foto1`, `foto2`, `foto3`) VALUES
('Cadeira', 'Uma Cadeira muito boa e bonita', 'ambientada_9.jpg', 'ambientada_9.jpg', 'ambientada_9.jpg'),
('Cadeira', 'Uma Cadeira muito boa e bonita', 'ambientada_9.jpg', 'ambientada_9.jpg', 'ambientada_9.jpg'),
('nvj j ', ' bjvkv', 'cadeira.jpg', 'cadeira.jpg', ''),
('', '', 'cadeira.jpg', 'cadeira.jpg', 'cadeira.jpg'),
('Gamers', 'Melhores imagens gamers para você', 'controledevideogame.jpg', 'download.jpg', 'mea-deluxe-edition-key-art.jpg.adapt.crop1x1.767p.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
