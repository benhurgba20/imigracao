<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head><style>
  body{
  margin: 0;
  padding: 0;
  font-family: sans-serif;
  background: url(bg.jpg) no-repeat;
  background-size: cover;
  
}
.login-box{
  width: 310px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  color: white;
}
.login-box h1{
  float: left;
  font-size: 40px;
  border-bottom: 6px solid #4caf50;
  margin-bottom: 50px;
  padding: 13px 0;
}
.textbox{
  width: 100%;
  font-size: 20px;
  padding: 8px 0;
  margin: 8px 0;
}
.textbox input{
  background: none;
  color: white;
  font-size: 18px;
  width: 20%;
}
.btn{
  width: 100%;
  background: none;
  border: 2px solid #4caf50;
  color: white;
  padding: 5px;
  font-size: 18px;
  cursor: pointer;
  margin: 12px 0;
}
p{
	 border: none;
  outline: none;
  background: none;
  color: white;
  font-size: 18px;
  width: 80%;
  float: left;
  margin: 0 10px;
}
div.login-box{
	background-color:rgba(0,0,0,0.6);
	margin:30px;
	padding:30px;
}
</style>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
<div class="login-box">
  <h1>Pré-Cadastro</h1>
  <a href="cadastrousuario.php">
    <button type="submit" class="btn" >Paciente</button></a>
  <a href="cadastronutricionista.php">
    <button type="submit" class="btn" >Nutricionista</button></a>
<p>Já possui cadastro?
<p><a href="index.php">Clique aqui</a>
</div>
  </body>
</html>