<?php
session_start();
?>
<!DOCTYPE html>
<html>
    
<head>
	<meta charset="utf8">
    <title>Nutri-U</title>
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery-1.2.6.pack.js"></script>
    <script type="text/javascript" src="assets/js/jquery.maskedinput-1.1.4.pack.js"/></script>
    <script type="text/javascript">
	$(document).ready(function(){
		$("#cpf").mask("999.999.999-99");
        $("#telefone").mask("(99) 99999-9999");
	});
</script>
</head>
 
<body>
        <div class="login-box">
                    <h1>Cadastro de Nutricionista</h1>
                    <?php
					if(isset($_SESSION['status_cadastro'])):
					?>
					<div class="notification is-success">
                      <p>Cadastro efetuado!</p>
                      <p>Faça login informando o seu usuário e senha <a href="login.php">aqui</a></p>
                    </div>
					<?php
					endif;
					unset($_SESSION['status_cadastro']);
					?>
					<?php
					if(isset($_SESSION['usuario_existe'])):
					?>
                    <div class="notification is-info">
                        <p>O usuário escolhido já existe. Informe outro e tente novamente.</p>
                    </div>
					<?php
					endif;
					unset($_SESSION['usuario_existe']);
					?>
                        <form action="cadastrarnutricionista.php" method="POST">
                            <div class="textbox">
                                    <input name="nome" type="text" placeholder="Nome" autofocus>
                            </div>
                            <div class="textbox">  
                                    <input name="usuario" type="text" placeholder="Usuario">    
                            </div>
                            <div class="textbox">  
                                    <input name="senha" type="password" placeholder="Senha">
                            </div>
                            <div class="textbox">  
                                    <input id="cpf"name="senha" type="text" placeholder="CPF">
                            </div>
                            <div class="textbox">  
                                    <input id="telefone" name="senha" type="text" placeholder="Telefone">
                            </div>
                            <div class="textbox">  
                                    <input name="senha" type="text" placeholder="CNN">
                            </div>
                            <button type="submit" class="btn">Cadastrar</button>
                        </form>
						<p><a href="index.html">Voltar</a>
                </div>
            
        
    
</body>
 
</html>