<?php
session_start();
?>
<!DOCTYPE html>
<html>
    
<head>
	<meta charset="utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nutri-U</title>
    <link rel="stylesheet" href="style.css">
</head>
 
<body>
        <div class="login-box">
                    <h1>Cadastro de Paciente</h1>
                    <?php
					if(isset($_SESSION['status_cadastro'])):
					?>
					<div class="notification is-success">
                      <p>Cadastro efetuado!</p>
                      <p>Faça login informando o seu usuário e senha <a href="login.php">aqui</a></p>
                    </div>
					<?php
					endif;
					unset($_SESSION['status_cadastro']);
					?>
					<?php
					if(isset($_SESSION['usuario_existe'])):
					?>
                    <div class="notification is-info">
                        <p>O usuário escolhido já existe. Informe outro e tente novamente.</p>
                    </div>
					<?php
					endif;
					unset($_SESSION['usuario_existe']);
					?>
                        <form action="cadastrarusuario.php" method="POST">
                            <div class="textbox">
                                    <input name="nome" type="text" placeholder="Nome" autofocus>
                            </div>
                            <div class="textbox">  
                                    <input name="usuario" type="text" placeholder="Usuario">    
                            </div>
                            <div class="textbox">  
                                    <input name="senha" type="password" placeholder="Senha">
                            </div>
                            <button type="submit" class="btn">Cadastrar</button>
                        </form>
                </div>
            
        
    
</body>
 
</html>