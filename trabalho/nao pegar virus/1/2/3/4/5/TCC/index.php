<?php
session_start();
?>

<!DOCTYPE html>
<html>
    
<head>
	<meta charset="utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nutri-U</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
        <div class="login-box">
            
                
                    <h1>LOGIN</h1>
                    <?php
                    if(isset($_SESSION['nao_autenticado'])):
                    ?>                    
                      <p>ERRO: Usuário ou senha inválidos.</p>
                    <?php
                    endif;
                    unset($_SESSION['nao_autenticado']);
                    ?>
                        <form action="login.php" method="POST">               
						<div class="textbox">
							<i class="fas fa-user"></i>
                            <input name="usuario" name="text" placeholder="Seu usuário" autofocus="">
                        </div>
                        <div class="textbox">
							<i class="fas fa-lock"></i>
                            <input name="senha" type="password" placeholder="Sua senha">
                        </div>
                            <button type="submit" class="btn">Entrar</button>
                        </form>
		<p>Não possui cadastro?
		<p><a href="precadastro.php">Cadastre-se aqui</a>
		</div>
</body>

</html>